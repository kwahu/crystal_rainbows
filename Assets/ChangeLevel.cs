﻿using UnityEngine;
using System.Collections;

public class ChangeLevel : MonoBehaviour {

	public string name;

	void OnCollisionStay2D (Collision2D col)
	{
		if (col.gameObject.layer == LayerMask.NameToLayer ("Player")) {
			Application.LoadLevel(name);
		}
	}
}
