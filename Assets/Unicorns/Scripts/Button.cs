﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

	public GameObject target;

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (coll.gameObject.layer == LayerMask.NameToLayer ("Player") ) {
			target.GetComponent<Door>().Move();
		}
	}
}
