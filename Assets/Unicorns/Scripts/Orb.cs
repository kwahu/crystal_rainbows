﻿using UnityEngine;
using System.Collections;

public class Orb : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.gameObject.layer == LayerMask.NameToLayer ("Player")) {
			Orbs.Add ();
			Destroy(this.gameObject);
		}
	}
}
