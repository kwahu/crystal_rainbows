﻿using UnityEngine;
using System.Collections;

public class Life : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.gameObject.layer == LayerMask.NameToLayer ("Player")) {
			Lives.Add ();
			Destroy(this.gameObject);
		}
	}
}
