﻿using UnityEngine;
using System.Collections;

public class EnemyMelee : MonoBehaviour
{
	
		public GameObject[] navPoints;
		Vector3[] navPositions;
		public float speed = 0.1f;
		int point = 0;
		Animator anim;
	
		void Start ()
		{
				anim = GetComponent<Animator> ();

				navPositions = new Vector3[navPoints.Length];
				for (int count = 0; count < navPoints.Length; count ++) {
						navPositions [count] = navPoints [count].transform.position;
						Debug.Log (navPositions [count]);
				}
		}

		void Update ()
		{
		Vector2 vec = rigidbody2D.GetVector (Vector2.zero);
				if (vec.x < 0) {
						AnimRunLeft ();
						Debug.Log ("AnimRunLeft ()");
				} else if (vec.x > 0) {
			AnimRunRight ();
						Debug.Log ("AnimRunRight ()");
				}
		}

		void FixedUpdate ()
		{
				if (Vector3.Distance (this.transform.position, navPositions [point]) > 0.1f) {
						Vector2 pos = Vector3.MoveTowards (transform.position, navPositions [point], speed);
						transform.position = pos;
				} else {
						NextPoint ();
				}
		}

		void NextPoint ()
		{
				point += 1;
				if (point == navPoints.Length)
						point = 0;
		}

		void AnimAttack ()
		{
				anim.Play ("attack");
		}

		void AnimRunRight ()
		{
				anim.Play ("run");
				transform.localScale = new Vector3 (1, 1, 1);
		}
	
		void AnimRunLeft ()
		{
				anim.Play ("run");
				transform.localScale = new Vector3 (-1, 1, 1);
		}

		void OnCollisionEnter2D (Collision2D col)
		{
				if (col.gameObject.layer == LayerMask.NameToLayer ("Player")) {
						Lives.Remove ();
				}
		}
}
