﻿using UnityEngine;
using System.Collections;

public class Destructible : MonoBehaviour
{

		public int hits = 1;

		void OnCollisionStay2D (Collision2D col)
		{
				if (col.gameObject.layer == LayerMask.NameToLayer ("Player")) {
					if (col.gameObject.GetComponent<PlayerControl2> ().attacking) {
								hits -= 1;
								if (hits == 0)
										Destroy (this.gameObject);
						}
				}
		}
}
