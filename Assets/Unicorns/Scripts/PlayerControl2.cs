﻿using UnityEngine;
using System.Collections;

public class PlayerControl2 : MonoBehaviour
{
		Animator anim;
		public float moveForce = 1000;
		public float airForce = 30;
		public float jumpForce = 4000;
		public bool grounded;
		public bool playingAttack = false;
		public bool attacking = false;
		public bool talking = false;
		public float teleportCoolDown = 0;

		void Start ()
		{
				anim = GetComponent<Animator> ();
		}

		void Update ()
		{
				attacking = false;
				if (!playingAttack) {
						if (Input.GetKey (KeyCode.RightArrow))
								AnimRunRight ();
						else if (Input.GetKey (KeyCode.LeftArrow))
								AnimRunLeft ();
						else
								anim.Play ("idle");
				}


		}

		void FixedUpdate ()
		{
				float h = Input.GetAxis ("Horizontal");

				if (!talking) {
						if (grounded) {
								if (Input.GetButtonDown ("Jump"))
										Jump ();
								MoveOnGround (h);
						} else 
								MoveInAir (h);

						if (Input.GetButtonDown ("Fire1")) {
								playingAttack = true;
								AnimAttack ();
						}
				}

				if (teleportCoolDown > 0)
						teleportCoolDown -= 1;
		}

		public bool CanTeleport ()
		{
				if (teleportCoolDown > 0)
						return false;
				else
						return true;
		}

		public void Teleport ()
		{
				teleportCoolDown = 20;
		}

		void MoveOnGround (float h)
		{
				rigidbody2D.AddForce (Vector2.right * h * moveForce, ForceMode2D.Impulse);
			//	rigidbody2D.drag = 100;
		}

		void MoveInAir (float h)
		{
				rigidbody2D.AddForce (Vector2.right * h * airForce, ForceMode2D.Impulse);
			//	rigidbody2D.drag = 1;
		}

		void Jump ()
		{
				rigidbody2D.AddForce (Vector2.up * jumpForce);
		}

		void AnimRunRight ()
		{
				anim.Play ("run");
				transform.localScale = new Vector3 (1, 1, 1);
		}

		void AnimRunLeft ()
		{
				anim.Play ("run");
				transform.localScale = new Vector3 (-1, 1, 1);
		}

		void AnimAttack ()
		{
				anim.Play ("attack");
		}

		void FinishAttack ()
		{
				playingAttack = false;
				attacking = true;
		}

		void OnCollisionStay2D (Collision2D col)
		{
				if (col.gameObject.layer == LayerMask.NameToLayer ("Ground"))
						grounded = true;
		}

		void OnCollisionExit2D (Collision2D col)
		{
				if (col.gameObject.layer == LayerMask.NameToLayer ("Ground"))
						grounded = false;
		}
}
