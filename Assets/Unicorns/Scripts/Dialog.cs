﻿using UnityEngine;
using System.Collections;

public class Dialog : MonoBehaviour
{

		public string[] line;
		int number = -1;
		public GameObject puff;
		public TextMesh text;
		GameObject player;

		void Update ()
		{
				if (number == 0) {
						puff.SetActive (true);
						player.GetComponent<PlayerControl2> ().talking = true;
				}

				if (puff.activeSelf && Input.GetKeyUp (KeyCode.Space)) {
						number += 1;
				}

				if (number == line.Length) {
						puff.SetActive (false);
						player.GetComponent<PlayerControl2> ().talking = false;
				}

				if (number >= 0 && number < line.Length)
						text.text = line [number];
		}

		void OnTriggerEnter2D (Collider2D coll)
		{
				if (coll.gameObject.layer == LayerMask.NameToLayer ("Player") && !puff.activeSelf) {
						number = 0;
						player = coll.gameObject;
				}
		}
}
