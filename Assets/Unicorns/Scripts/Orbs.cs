﻿using UnityEngine;
using System.Collections;

public class Orbs : MonoBehaviour {

	//public int startingLives = 3;
	static int orbs;
	public int maxOrbs = 5;
	public GameObject[] images;

	// Use this for initialization
	void Start () {
		//lives = startingLives;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log (orbs);
		for (int count = 0; count < maxOrbs; count ++) {
			if(count<orbs)
				images[count].SetActive(true);
			else
				images[count].SetActive(false);
				}
	}

	/*public static void Remove()
	{
		lives -= 1;
	}*/

	public static void Add()
	{
		orbs += 1;
	}

	public static int Get()
	{
		return orbs;
	}
}
