﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {

	public GameObject target;

	void OnTriggerEnter2D (Collider2D col)
	{
		if (target && col.gameObject.layer == LayerMask.NameToLayer ("Player") && col.GetComponent<PlayerControl2>().CanTeleport() ) {
			col.transform.position = target.transform.position;
			col.GetComponent<PlayerControl2>().Teleport();
				}
	}
}
