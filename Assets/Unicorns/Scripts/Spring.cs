﻿using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour {

	public Vector2 jumpForce;

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.layer == LayerMask.NameToLayer ("Player"))
						col.transform.rigidbody2D.AddForce (jumpForce);
	}
}
