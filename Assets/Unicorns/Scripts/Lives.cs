﻿using UnityEngine;
using System.Collections;

public class Lives : MonoBehaviour {

	public static int startingLives = 3;
	static int lives;
	public int maxLives = 5;
	public float imageSpacing = 0.5f;
	public GameObject[] images;

	// Use this for initialization
	void Start () {
		lives = startingLives;
	}
	
	// Update is called once per frame
	void Update () {
		for (int count = 0; count < maxLives; count ++) {
			if(count<lives)
				images[count].SetActive(true);
			else
				images[count].SetActive(false);
				}
	}

	public static void Remove()
	{
		lives -= 1;
	}

	public static void Add()
	{
		lives += 1;
	}

	public static int Get()
	{
		return lives;
	}
}
