﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public Vector3 transition;
	bool move = false;
	public bool oneTime = false;
	bool done = false;

	void Start()
	{
		transition = this.transform.position + transition;
		}

	void Update () {
		if (move && !done) {
			transform.position = Vector3.MoveTowards (transform.position, transition, 0.1f);
			if( transform.position == transition && oneTime)
				done = true;
				}
	}

	public void Move()
	{
		move = true;
	}
}
